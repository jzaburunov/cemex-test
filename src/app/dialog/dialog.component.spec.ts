import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatDialogModule,
    MatDialogRef,
    MAT_DIALOG_DATA
} from '@angular/material/dialog';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material/material.module';
import { findComponent } from '../utils/tests';
import { DialogComponent } from './dialog.component';

describe('DialogComponent', () => {
    let fixture: ComponentFixture<DialogComponent>;
    let component: DialogComponent;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [DialogComponent],
            schemas: [NO_ERRORS_SCHEMA],
            imports: [
                BrowserModule,
                FormsModule,
                ReactiveFormsModule,
                MaterialModule,
                BrowserAnimationsModule
            ],
            providers: [
                {
                    provide: 'dialog-overview-app-dialog',
                    useValue: DialogComponent
                },
                { provide: MatDialogRef, useValue: {} },
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: {
                        status: []
                    }
                }
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(DialogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        const app = fixture.componentInstance;
        expect(app).toBeTruthy();
    });

    describe('status section', () => {
        it('should render the section', () => {
            const status = findComponent(fixture, '.app-section');
            expect(status).toBeTruthy();
        });

        it('should render three checkboxes', () => {
            const status = findComponent(fixture, '.app-section');
            expect(
                (status.nativeElement as HTMLElement).querySelectorAll(
                    'mat-checkbox'
                ).length
            ).toBe(3);
        });
    });

    it('should render Phase and Month section', () => {
        const phaseAndMonth = findComponent(fixture, '.select-row');
        expect(phaseAndMonth).toBeTruthy();
        expect(
            (phaseAndMonth.nativeElement as HTMLElement).childElementCount
        ).toBe(2);
    });

    it('should render Cancel and Apply buttons', () => {
        const buttons = findComponent(fixture, '.dialog-buttons');
        expect(buttons).toBeTruthy();
        expect((buttons.nativeElement as HTMLElement).childElementCount).toBe(
            2
        );
    });
});
