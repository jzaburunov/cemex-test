import { Component, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
    ContractStatusEnum,
    Filters,
    Month,
    Phase
} from '../_interface/contract.model';

@Component({
    selector: 'dialog-overview-app-dialog',
    templateUrl: 'dialog.component.html',
    styleUrls: ['./dialog.component.css']
})
export class DialogComponent {
    status: FormGroup;
    month = new FormControl();
    phase = new FormControl();
    phaseOptions = Object.keys(Phase);
    monthOptions = Object.keys(Month);

    constructor(
        fb: FormBuilder,
        public dialogRef: MatDialogRef<DialogComponent>,
        @Inject(MAT_DIALOG_DATA)
        public data: Required<Omit<Filters, 'supplierName'>>
    ) {
        this.status = fb.group({
            0: data.status.includes(ContractStatusEnum.ACTIVE),
            1: data.status.includes(ContractStatusEnum.PENDING_APPROVAL),
            2: data.status.includes(ContractStatusEnum.WAITING_COMPENSATION)
        });
        this.phase = fb.control(data.phase);
        this.month = fb.control(data.month);
    }

    onCancel(): void {
        this.dialogRef.close();
    }

    onApply(): void {
        const statusVal = Object.keys(this.status.value).reduce(
            (agr, currKey) => {
                if (this.status.value[currKey])
                    return [...agr, Number(currKey)];
                return agr;
            },
            [] as number[]
        );

        this.dialogRef.close({
            status: statusVal,
            phase: this.phase.value ?? [],
            month: this.month.value ?? []
        });
    }
}
