import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { findComponent } from './utils/tests';

describe('AppComponent', () => {
    let fixture: ComponentFixture<AppComponent>;
    let component: AppComponent;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [AppComponent],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();

        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the app', () => {
        const app = fixture.componentInstance;
        expect(app).toBeTruthy();
    });

    describe('total cost', () => {
        it(`should have been set`, () => {
            const app = fixture.componentInstance;
            expect(app.totalCost).toBeTruthy();
        });

        it(`should have been rendered`, () => {
            const totalEl = findComponent(fixture, '.total-wrapper')
                .nativeElement as HTMLElement;
            expect(totalEl.querySelector('h1')?.textContent).toBe(
                'Total: $28,410.00 USD'
            );
        });
    });

    it('should render title', () => {
        const compiled = fixture.nativeElement as HTMLElement;
        expect(compiled.querySelector('p')?.textContent).toContain(
            'Financial Management • CAPEX Management'
        );
        expect(compiled.querySelector('h1')?.textContent).toContain(
            'CEMEX Go Online Stores'
        );
    });

    describe('table', () => {
        it(`should have been rendered`, () => {
            const tableEl = findComponent(fixture, 'table');
            expect(tableEl).toBeTruthy();
        });
    });
});
