import { Component, OnInit, Optional } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { MatTableDataSource } from '@angular/material/table';

import {
    Contract,
    ContractStatusEnum,
    Filters,
    Month,
    Phase
} from './_interface/contract.model';

import MOCK_DATA from './_mockData';

const stringPredicate = (
    namePart: string | undefined | null,
    dataString: string
): boolean =>
    namePart === undefined ||
    namePart === null ||
    dataString.toLowerCase().includes(namePart.toLowerCase());

const oneOfPredicate = (
    filters: (ContractStatusEnum | Month | Phase)[] | undefined | null,
    dataStatus: ContractStatusEnum | Month | Phase
): boolean =>
    filters === undefined ||
    filters === null ||
    filters.length === 0 ||
    Boolean(filters.includes(dataStatus));

function customFilter(
    data: Contract,
    filter: string // NOTE: Need to keep it as string, because of typings of the framework
): boolean {
    const filters: Filters = JSON.parse(filter);
    return (
        stringPredicate(filters.supplierName, data.supplierName) &&
        oneOfPredicate(filters.status, data.status) &&
        oneOfPredicate(filters.phase, data.phase) &&
        oneOfPredicate(filters.month, data.month)
    );
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    // Table
    dataSource = new MatTableDataSource<Contract>();
    public displayedColumns = [
        'status',
        'supplierName',
        'month',
        'phase',
        'internalOrder',
        'amount'
    ] as (keyof Contract)[];

    // Total cost
    public get totalCost() {
        return this.dataSource.filteredData.reduce(
            (acc, row) => acc + Number(row.amount),
            0
        );
    }

    constructor(@Optional() public dialog: MatDialog) {}

    ngOnInit() {
        this.dataSource.data = MOCK_DATA;

        this.dataSource.filterPredicate = customFilter;
    }

    // Table filtering
    public filterTableRows = (value: Filters) => {
        this.dataSource.filter = JSON.stringify(value);
    };
}
