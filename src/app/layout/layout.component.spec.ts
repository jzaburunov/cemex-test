import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { findComponent } from '../utils/tests';
import { LayoutComponent } from './layout.component';

describe('LayoutComponent', () => {
    let fixture: ComponentFixture<LayoutComponent>;
    let component: LayoutComponent;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [LayoutComponent],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();

        fixture = TestBed.createComponent(LayoutComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        const app = fixture.componentInstance;
        expect(app).toBeTruthy();
    });

    it('should render site head', () => {
        const head = findComponent(fixture, '.site-head');
        expect(head).toBeTruthy();
    });
});
