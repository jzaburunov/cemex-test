import {
    Contract,
    ContractStatusEnum,
    Month,
    Phase
} from './_interface/contract.model';

export default [
    {
        supplierName: 'Jakub Zavazal',
        month: Month.March,
        phase: Phase.Deployment,
        internalOrder: '10024234',
        amount: '5120.00',
        status: ContractStatusEnum.ACTIVE,
        statusText: 'Active'
    },
    {
        supplierName: 'Jonathan Holden',
        month: Month.April,
        phase: Phase.Research,
        internalOrder: '10024299',
        amount: '5120.00',
        status: ContractStatusEnum.ACTIVE,
        statusText: 'Active'
    },
    {
        supplierName: 'Miguel Zavala',
        month: Month.March,
        phase: Phase.Deployment,
        internalOrder: '10024211',
        amount: '4430.00',
        status: ContractStatusEnum.PENDING_APPROVAL,
        statusText: 'Pending Approval'
    },
    {
        supplierName: 'Vlad Titis Tudor',
        month: Month.February,
        phase: Phase.Development,
        internalOrder: '10024267',
        amount: '3320.00',
        status: ContractStatusEnum.PENDING_APPROVAL,
        statusText: 'Pending Approval'
    },
    {
        supplierName: 'Aleksey Romanyuk',
        month: Month.February,
        phase: Phase.Development,
        internalOrder: '10024223',
        amount: '5120.00',
        status: ContractStatusEnum.WAITING_COMPENSATION,
        statusText: 'Waiting Compensation'
    },
    {
        supplierName: 'Carlos Froncisco Rocha Ceballos',
        month: Month.April,
        phase: Phase.Ideation,
        internalOrder: '10032234',
        amount: '5300.00',
        status: ContractStatusEnum.WAITING_COMPENSATION,
        statusText: 'Waiting Compensation'
    }
] as Contract[];
