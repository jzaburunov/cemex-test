export enum ContractStatusEnum {
    ACTIVE,
    PENDING_APPROVAL,
    WAITING_COMPENSATION
}

export enum Month {
    January = 'January',
    February = 'February',
    March = 'March',
    April = 'April',
    May = 'May',
    June = 'June',
    July = 'July',
    August = 'August',
    September = 'September',
    October = 'October',
    November = 'November',
    December = 'December'
}

export enum Phase {
    Deployment = 'Deployment',
    Development = 'Development',
    Research = 'Research',
    Ideation = 'Ideation'
}

export type Contract = {
    status: ContractStatusEnum;
    statusText: string;
    supplierName: string;
    month: Month;
    phase: Phase;
    internalOrder: string;
    amount: string;
};

export type Filters = {
    supplierName: string;
    status: ContractStatusEnum[];
    phase: Phase[];
    month: Month[];
};
