import MOCK_DATA from '../_mockData';
import { Component, Input, OnInit, Optional } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, startWith, map } from 'rxjs';
import {
    ContractStatusEnum,
    Filters,
    Month,
    Phase
} from '../_interface/contract.model';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import { AppComponent } from '../app.component';

const filterSearchOptions = (value: string, options: string[]): string[] => {
    const filterValue = value.toLowerCase();

    return options.filter((option) =>
        option.toLowerCase().includes(filterValue)
    );
};

enum statusDict {
    'Active',
    'Pending Approval',
    'Waiting Compensation'
}

@Component({
    selector: 'app-filters-form',
    templateUrl: './filters-form.component.html',
    styleUrls: ['./filters-form.component.css']
})
export class FiltersFormComponent implements OnInit {
    @Input()
    table: AppComponent | null = null;

    // Search Field
    searchField = new FormControl();
    options: string[] = MOCK_DATA.map((contract) => contract.supplierName);
    filteredOptions?: Observable<string[]>;

    public filterByName = (value: string) => {
        this.nameFilter = value.trim();
        this.filterTableRows({
            supplierName: this.nameFilter,
            status: this.statusFilters,
            phase: this.phaseFilter,
            month: this.monthFilter
        });
    };

    // Filters
    filters: (ContractStatusEnum | Phase | Month)[] = []; // Array for chips
    statusFilters: ContractStatusEnum[] = [];
    phaseFilter: Phase[] = [];
    monthFilter: Month[] = [];
    nameFilter = '';

    // Table filtering
    public filterTableRows = (value: Filters) => {
        if (this.table) {
            this.table.filterTableRows(value);
        }
    };

    constructor(@Optional() public dialog: MatDialog) {}

    ngOnInit(): void {
        this.filteredOptions = this.searchField.valueChanges.pipe(
            startWith(''),
            map((value) => filterSearchOptions(value, this.options))
        );
    }

    openDialog(): void {
        const dialogData = {
            status: this.statusFilters,
            phase: this.phaseFilter,
            month: this.monthFilter
        };
        const dialogRef = this.dialog.open<
            DialogComponent,
            typeof dialogData,
            typeof dialogData
        >(DialogComponent, { data: dialogData });

        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this.statusFilters = result.status;
                this.phaseFilter = result.phase;
                this.monthFilter = result.month;
                this.filters = [
                    ...result.status,
                    ...result.phase,
                    ...result.month
                ];
                this.filterTableRows({
                    status: this.statusFilters,
                    phase: this.phaseFilter,
                    month: this.monthFilter,
                    supplierName: this.nameFilter
                });
            }
        });
    }

    removeChip(filter: number | string): void {
        this.statusFilters = this.statusFilters.filter(
            (value) => value !== filter
        );
        this.phaseFilter = this.phaseFilter.filter((value) => value !== filter);
        this.monthFilter = this.monthFilter.filter((value) => value !== filter);
        this.filters = this.filters.filter((value) => value !== filter);
        this.filterTableRows({
            status: this.statusFilters,
            phase: this.phaseFilter,
            month: this.monthFilter,
            supplierName: this.nameFilter
        });
    }

    public chipTitle(filter: FiltersFormComponent['filters'][number]): string {
        if (Object.keys(Phase).includes(filter as Phase)) {
            return `Phase: ${filter}`;
        }
        if (Object.keys(Month).includes(filter as Month)) {
            return `Month: ${filter}`;
        }
        if (Object.keys(ContractStatusEnum).includes(String(filter))) {
            return `Status: ${
                statusDict[filter as unknown as `${ContractStatusEnum}`]
            }`;
        }
        return filter as string;
    }

    clearFilters(): void {
        this.statusFilters = [];
        this.phaseFilter = [];
        this.monthFilter = [];
        this.filters = [];
        this.nameFilter = '';
        this.searchField.setValue('');
        this.filterTableRows({
            status: this.statusFilters,
            phase: this.phaseFilter,
            month: this.monthFilter,
            supplierName: this.nameFilter
        });
    }
}
