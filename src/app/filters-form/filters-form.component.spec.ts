import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material/material.module';
import { findComponent } from '../utils/tests';
import { FiltersFormComponent } from './filters-form.component';

describe('FiltersFormComponent', () => {
    let fixture: ComponentFixture<FiltersFormComponent>;
    let component: FiltersFormComponent;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [FiltersFormComponent],
            schemas: [NO_ERRORS_SCHEMA],
            imports: [MaterialModule, BrowserAnimationsModule]
        }).compileComponents();

        fixture = TestBed.createComponent(FiltersFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        const formComponent = fixture.componentInstance;
        expect(formComponent).toBeTruthy();
    });

    it('should render search field', () => {
        const head = findComponent(fixture, '.input-wrapper input');
        expect(head).toBeTruthy();
    });

    it('should render filters button', () => {
        const head = findComponent(fixture, '.button-wrapper button');
        expect(head).toBeTruthy();
    });

    it('should not render chip list', () => {
        const head = findComponent(fixture, '.app-chip-list');
        expect(head).toBeFalsy();
    });

    it('should not render clear filters button', () => {
        const head = findComponent(fixture, '.button-clear-filters-wrapper');
        expect(head).toBeFalsy();
    });
});
