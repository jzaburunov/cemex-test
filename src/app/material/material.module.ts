import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        MatCheckboxModule,
        MatIconModule,
        MatChipsModule,
        MatTabsModule,
        MatSelectModule,
        MatAutocompleteModule,
        MatDialogModule,
        MatTableModule,
        MatFormFieldModule,
        MatInputModule
    ],
    exports: [
        MatCheckboxModule,
        MatIconModule,
        MatChipsModule,
        MatTabsModule,
        MatSelectModule,
        MatDialogModule,
        MatTableModule,
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule
    ]
})
export class MaterialModule {}
