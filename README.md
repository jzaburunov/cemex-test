# CEMEX app

Example of Angular application for CEMEX

Requirements:

-   Use Angular framework

-   Create a project that reflects screenshots

-   The project must be responsive

-   Use a Git repo (github, bitbucket) as version control

## Quick start

1. After clonning of the repository, run this command in a terminal in a project directory : `npm i && ng serve --open`

2. The page should be opened automatically when run is done, if not - navigate to `http://localhost:4200/`.

## Results

This is how it looks like

### Desktop view

![plot](./docs/desktop.png)

### Mobile view

![plot](./docs/mobile.png)
